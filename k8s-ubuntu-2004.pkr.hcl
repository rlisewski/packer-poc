source "qemu" "ubuntu-2004" {
  iso_url           = "https://cloud-images.ubuntu.com/releases/focal/release/ubuntu-20.04-server-cloudimg-amd64-disk-kvm.img"
  iso_checksum      = "sha256:3210c21041c09cf7921377b6d753c716c498be2174dba4fd5959aeeaf244fd44"
  output_directory  = var.output_dir_name
  shutdown_command  = "echo 'packer' | sudo -S shutdown -P now"
  disk_size         = "5000M"
  format            = "qcow2"
  disk_image = true
  accelerator       = "kvm"
  ssh_username      = var.user
  ssh_password      = var.password
  ssh_timeout       = "2m"
  ssh_port = 22
  vm_name           = "${var.image_name}.qcow2"
  disk_interface    = "virtio"
  boot_wait         = "10s"
  disk_compression = true
  headless = true
  use_default_display = false
  boot_command = ["<enter>"]
  qemuargs = [
                ["-m", var.mem],
                ["-smp", "cpus=${var.cpu}"],
                ["-cdrom", var.cloud_init_image],
                ["-serial", "mon:stdio"]
            ]
}

build {
  name = "prepare-ubuntu-2004"
  sources = ["source.qemu.ubuntu-2004"]
  provisioner "shell" {
    environment_vars = [
      "USER_NAME=${var.user}",
    ]
    
    scripts = [
      "./scripts/docker_install.sh"
    ]
  }

  post-processor "shell-local" {
    inline = [
      "mv ./${var.output_dir_name}/${var.image_name}.qcow2 /home/rafalski/playroom/cache_images/",
      "rmdir ./${var.output_dir_name}"
    ]
  }

}



variable "cloud_init_image" {
  type    = string
  default = "cloud-init.img"
}

variable "user" {
  type    = string
  default = "ubuntu"
}

variable "password" {
  type    = string
  default = "s0m3password"
}

variable "mem" {
  type    = string
  default = "4G"
}
variable "cpu" {
  type    = string
  default = "4"
}

variable "image_name" {
  type = string
  default = "ubuntu-2004"
}

variable "output_dir_name" {
  type = string
  default = "output_ubuntu"
}
