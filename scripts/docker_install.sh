#!/usr/bin/env bash

sudo apt update -qq
sudo apt-get remove -yyq docker docker-engine docker.io containerd runc || true
sudo apt-get install -yyq apt-transport-https ca-certificates curl gnupg lsb-release
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --batch --yes --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
echo "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt update
sudo apt install -yqq docker-ce docker-ce-cli containerd.io
sudo apt clean
sudo groupadd docker || true
sudo usermod -aG docker ${USER_NAME}
